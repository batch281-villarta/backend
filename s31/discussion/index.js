// Using require directive to load http module
const http = require('http');

// Create a server
http
  .createServer((req, res) => {
    // Returning what type of response being thrown to the client
    res.writeHead(200, { 'Content-type': 'text/plain' });

    // Send the response with the text content 'Hello, world'
    res.end('Hello, world');
  })
  .listen(4000);

console.log('Server listening on 4000');
