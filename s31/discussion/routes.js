const http = require('http');

const port = 4000;

const app = http.createServer((req, res) => {
  console.log(req.url);

  if (req.url === '/greeting') {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end('<h1>Hello Again</h1>');
  } else if (req.url === '/homepage' || req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('This is the homepage');
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Page not found');
  }
});

app.listen(port);

console.log(`Server now accessible at localhost:${port}`);
