const trainer = {
  name: "Ash Ketchum",
  age: 26,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
  },
  talk() {
    console.log(`${this.pokemon[0]}! I choose you!`);
  },
};

console.log(trainer.name);
console.log(trainer["pokemon"]);
trainer.talk();

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  this.tackle = function (target) {
    target.health -= this.attack;

    console.log(`${this.name} tackled ${target.name}`);
    console.log(`${target.name} health is now reduced to ${target.health}`);

    if (target.health <= 0) {
      this.faint(target);
    }
  };

  this.faint = function (target) {
    console.log(`${target.name} has fainted.`);
  };
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

pikachu.tackle(geodude);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    trainer: typeof trainer !== "undefined" ? trainer : null,
    Pokemon: typeof Pokemon !== "undefined" ? Pokemon : null,
  };
} catch (err) {}
