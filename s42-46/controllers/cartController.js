const Product = require('../models/Product');

const catchAsync = require('../utils/catchAsync');
const auth = require('../middlewares/auth');
const AppError = require('../utils/appError');
const User = require('../models/User');

module.exports.getCart = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData == null)
    return next(new AppError('Invalid authorization token'));

  // if (userData.isAdmin)
  //   return next(new AppError('You must be a user to see your orders'));

  const user = await User.findById(userData.id).select(['addedToCart', '-_id']);

  const cart = user.addedToCart;

  if (!user.addedToCart.products.length || user.addedToCart == null)
    return res.status(403).json({ status: 'error', cart });

  res.status(200).json({ status: 'success', cart });
});

module.exports.addToCart = catchAsync(async (req, res, next) => {
  const userId = auth.decode(req.headers.authorization).id;
  const orderToCart = req.body;

  const product = await Product.findById(orderToCart.productId);

  const orderDetails = {
    productId: product._id,
    productName: product.name,
    image: product.image,
    description: product.description,
    quantity: orderToCart.quantity,
    price: product.price,
    subTotal: orderToCart.quantity * product.price
  };

  console.log(orderDetails);

  const user = await User.findById(userId);

  user.addedToCart.products.push(orderDetails);

  const totalAmount = user.addedToCart.products.reduce(
    (acc, product) => acc + product.subTotal,
    0
  );

  // const total =
  //   parseFloat(user.addedToCart.totalAmount) + parseFloat(totalAmount);
  console.log(user.addedToCart.products);

  user.addedToCart.totalAmount = totalAmount;

  await user.save();

  res.status(201).json({
    message: 'Added to cart successfully'
  });
});

// module.exports.removeProducts = catchAsync(async (req, res, next) => {
//   const userData = auth.decode(req.headers.authorization);

//   const user = await User.findById(userData.id);

//   if (!user.addedToCart.products.length)
//     return next(new AppError('Cart is empty'));

//   user.addedToCart.products = [];
//   user.addedToCart.totalAmount = 0;

//   await user.save();

//   res
//     .status(200)
//     .json({ message: 'Successfully remove all products from cart' });
// });

// module.exports.removeProducts = catchAsync(async (req, res, next) => {
//   const userData = auth.decode(req.headers.authorization);

//   const user = await User.findById(userData.id);

//   if (!user.addedToCart.products.length) {
//     return next(new AppError('Cart is empty'));
//   }

//   const productIdsToRemove = req.body;

//   const updatedProduct = user.addedToCart.products.filter(
//     (product) =>
//       !productIdsToRemove.some((item) => item.productId === product.productId)
//   );

//   // user.addedToCart.products

//   console.log(updatedProduct);

//   // Update totalAmount based on the remaining products in the cart
//   user.addedToCart.totalAmount = user.addedToCart.products.reduce(
//     (total, product) => total + product.price,
//     0
//   );

//   await user.save();

//   res.status(200).json({
//     message: `Successfully removed ${
//       req.body.length > 1 ? 'products' : 'product'
//     }  from cart`
//   });
// });

module.exports.removeProducts = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);
  const cartId = req.body.cartId;

  const user = await User.findById(userData.id);

  const updatedProducts = user.addedToCart.products.filter((product) => {
    return product._id.toString() !== cartId;
  });

  user.addedToCart.products = updatedProducts;

  const totalAmount = user.addedToCart.products.reduce(
    (acc, product) => acc + product.subTotal,
    0
  );
  // const total = parseInt(user.addedToCart.totalAmount) + parseInt(totalAmount);

  user.addedToCart.totalAmount = totalAmount;

  console.log(user.addedToCart.products);
  console.log(totalAmount);

  await user.save();

  res.status(200).json({
    cart: user.addedToCart.products,
    total: user.addedToCart.totalAmount
  });
});

module.exports.updateProducts = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);

  const user = await User.findById(userData.id);

  const productInCart = user.addedToCart.products;

  const productToUpdate = productInCart.find(
    (product) => product._id.toString() === req.body.cartId
  );

  productToUpdate.quantity = req.body.quantity;
  productToUpdate.subTotal = req.body.quantity * productToUpdate.price;

  const totalAmount = user.addedToCart.products.reduce(
    (acc, product) => acc + product.subTotal,
    0
  );

  user.addedToCart.totalAmount = totalAmount;

  console.log(productToUpdate);
  user.save();

  // res.status(200).json(user.addedToCart);

  res.status(200).json({
    cart: user.addedToCart.products,
    total: user.addedToCart.totalAmount
  });
});
