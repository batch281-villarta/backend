const mongoose = require('mongoose');

const User = require('../models/User');
const Product = require('../models/Product');

const bcrypt = require('bcrypt');
const auth = require('../middlewares/auth');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');
const { response } = require('express');

module.exports.registerUser = catchAsync(async (req, res, next) => {
  const { firstName, lastName, mobileNumber, email, password } = req.body;

  const existingUser = await User.findOne({ email });

  if (existingUser) {
    return next(new AppError(`User already exists. Try login instead!`));
  }

  const newUser = await User.create({
    firstName,
    lastName,
    mobileNumber,
    email,
    password: bcrypt.hashSync(password, 10)
  });

  newUser.password = '******';

  res
    .status(201)
    .json({ message: 'User registered successfully', data: newUser });
});

module.exports.loginUser = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  const result = await User.findOne({ email });

  if (result == null) {
    return next(new AppError(`Can't find user: ${email}`));
  }

  const isPasswordCorrect = bcrypt.compareSync(password, result.password);

  if (!isPasswordCorrect) {
    return next(new AppError('Password is incorrect'));
  }

  //success
  res.status(201).json({ access: auth.createAccessToken(result) });
});

module.exports.getUserDetails = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData == null)
    return next(new AppError('Invalid authorization token'));

  const user = await User.findById(req.params.userId).select([
    '-__v',
    '-addedToCart'
  ]);

  user.password = '******';
  return res.status(200).json(user);
});

module.exports.getAllUserDetails = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData == null)
    return next(new AppError('Invalid authorization token'));

  const user = await User.find().select(['-__v', '-addedToCart']);

  user.password = '******';
  return res.status(200).json(user);
});

module.exports.getLoginUserDetails = catchAsync(async (req, res, next) => {
  const userId = auth.decode(req.headers.authorization).id;

  console.log(req.headers.authorization);
  console.log(userId);

  const user = await User.findById(userId).select(['-__v']);

  user.password = '******';
  return res.status(200).json(user);
});

module.exports.checkout = catchAsync(async (req, res, next) => {
  const userId = auth.decode(req.headers.authorization).id;

  const user = await User.findById(userId);

  const products = user.addedToCart;

  user.orderedProduct.push(products);

  if (!user.addedToCart.products.length)
    return next(new AppError('Cart is empty'));

  //remove cart products when checkout

  await user.save().then(() => {
    const productsArray = user.addedToCart.products;
    console.log('products Array', productsArray);
    productsArray.map(async (product) => {
      const productDB = await Product.findById(product.productId);
      productDB.userOrders.push({
        userId,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email
      });

      await productDB.save();
    });

    user.addedToCart.products = [];
    user.addedToCart.totalAmount = 0;

    user.save();
  });

  res.status(201).json({
    message: 'Order created successfully',
    OrderedProduct: user.orderedProduct.at(-1)
  });
});

module.exports.getOrders = catchAsync(async (req, res, next) => {
  const orders = await User.find({ orderedProduct: { $ne: [] } }, {}).select([
    '-addedToCart',
    '-password',
    '-isAdmin',
    '-id',
    '-__v',
    '-orderedProduct._id',
    '-orderedProduct.products._id'
  ]);

  //success
  res.status(201).json({ results: orders.length, orders });
});

module.exports.getMyOrder = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData == null)
    return next(new AppError('Invalid authorization token'));

  // if (userData.isAdmin)
  //   return next(new AppError('You must be a user to see your orders'));

  const orders = await User.findById(userData.id).select([
    '-addedToCart',
    '-email',
    '-password',
    '-isAdmin',
    '-_id',
    '-__v',
    '-orderedProduct._id',
    '-orderedProduct.products._id'
  ]);

  res.status(200).json(orders);
});

module.exports.setAdmin = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.params.userId).select([
    '-orderedProduct -__v',
    '-addedToCart'
  ]);
  if (!user) {
    return next(new AppError('User not found', 404));
  }

  if (user.isAdmin) {
    return res.status(200).json({ message: 'The user is already an admin' });
  }

  user.isAdmin = true;
  await user.save();

  const allUsers = await User.find();

  user.password = '******';
  return res.status(200).json({ message: 'Success', users: allUsers });
});

module.exports.removeAdmin = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.params.userId).select([
    '-orderedProduct -__v',
    '-addedToCart'
  ]);
  if (!user) {
    return next(new AppError('User not found', 404));
  }

  if (!user.isAdmin) {
    return res.status(200).json({ message: 'The user is not and Admin!' });
  }

  user.isAdmin = false;
  await user.save();

  const allUsers = await User.find();

  user.password = '******';
  return res.status(200).json({ message: 'Success', users: allUsers });
});
