const Product = require('../models/Product');

//middlewares
const auth = require('../middlewares/auth');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');

// module.exports.addProduct = catchAsync(async (req, res, next) => {
//   const product = await Product.create(req.body);

//   res
//     .status(201)
//     .json({ message: 'Product added successfully', data: product });
// });

module.exports.addProduct = catchAsync(async (req, res, next) => {
  const products = req.body; // Assuming req.body is an array of products

  const createdProducts = await Promise.all(
    products.map((productData) => Product.create(productData))
  );

  const allProducts = await Product.find({}).select('-__v');
  // .sort({ createdOn: 1 });

  res.status(201).json({
    message: 'Products added successfully',
    products: allProducts
  });
});

// module.exports.getAllProducts = catchAsync(async (req, res, next) => {
//   const products = await Product.find({}).select('-__v');
//   res.status(200).json({ results: products.length, products });
// });

module.exports.getAllProducts = catchAsync(async (req, res, next) => {
  const sortOption = req.query.sortBy || 'name';
  const sortOrder = req.query.sortOrder || 'asc';

  const sort = {};
  sort[sortOption] = sortOrder === 'desc' ? -1 : 1;

  const products = await Product.find({}).select('-__v').sort(sort);
  res.status(200).json({ results: products.length, products });
});

module.exports.getFeaturedProducts = catchAsync(async (req, res, next) => {
  const sortOption = req.query.sortBy || 'name';
  const sortOrder = req.query.sortOrder || 'asc';

  const sort = {};
  sort[sortOption] = sortOrder === 'desc' ? -1 : 1;

  const products = await Product.find({ isFeatured: true })
    .select('-__v')
    .sort(sort);
  res.status(200).json({ results: products.length, products });
});

module.exports.getAllActive = async (req, res) => {
  const sortOption = req.query.sortBy || 'name';
  const sortOrder = req.query.sortOrder || 'asc';

  const sort = {};
  sort[sortOption] = sortOrder === 'desc' ? -1 : 1;

  const products = await Product.find({ isActive: true })
    .select('-__v')
    .sort(sort);
  res.status(200).json({ results: products.length, products });
};

module.exports.getProduct = catchAsync(async (req, res, next) => {
  const products = await Product.findById(req.params.productId).select('-__v');

  if (!products) return next(new AppError('Product not found', 404));

  res.status(200).json(products);
});

module.exports.updateProduct = catchAsync(async (req, res, next) => {
  const updatedProduct = await Product.findByIdAndUpdate(
    req.params.productId,
    req.body,
    { new: true }
  ).select('-__v');

  if (!updatedProduct) return next(new AppError('Product not found', 404));

  const allProducts = await Product.find({}).select('-__v');

  res
    .status(201)
    .json({ message: 'Product updated successfully', products: allProducts });
});

module.exports.archiveProduct = catchAsync(async (req, res, next) => {
  const archiveProduct = await Product.findByIdAndUpdate(
    req.params.productId,
    { isActive: false },
    { new: true }
  ).select('-__v');

  if (!archiveProduct) return next(new AppError('Product not found', 404));

  const allProducts = await Product.find({}).select('-__v');

  res
    .status(201)
    .json({ message: 'Product archived successfully', products: allProducts });
});

module.exports.activateProduct = catchAsync(async (req, res, next) => {
  const activatedProduct = await Product.findByIdAndUpdate(
    req.params.productId,
    { isActive: true },
    { new: true }
  ).select('-__v');

  if (!activatedProduct) return next(new AppError('Product not found', 404));

  const allProducts = await Product.find({}).select('-__v');

  res.status(201).json({
    message: 'Product activated successfully',
    products: allProducts
  });
});

module.exports.unfeatureProduct = catchAsync(async (req, res, next) => {
  const activatedProduct = await Product.findByIdAndUpdate(
    req.params.productId,
    { isFeatured: false }
  ).select('-__v');

  if (!activatedProduct) return next(new AppError('Product not found', 404));

  const allProducts = await Product.find({}).select('-__v');

  res.status(201).json({
    message: 'Product unfeatured successfully',
    products: allProducts
  });
});

module.exports.featureProduct = catchAsync(async (req, res, next) => {
  const activatedProduct = await Product.findByIdAndUpdate(
    req.params.productId,
    { isFeatured: true }
  ).select('-__v');

  if (!activatedProduct) return next(new AppError('Product not found', 404));

  const allProducts = await Product.find({}).select('-__v');

  res.status(201).json({
    message: 'Product featured successfully',
    products: allProducts
  });
});

module.exports.deleteProduct = catchAsync(async (req, res, next) => {
  const activatedProduct = await Product.findByIdAndDelete(
    req.params.productId
  );

  if (!activatedProduct) return next(new AppError('Product not found', 404));

  const allProducts = await Product.find({}).select('-__v');

  res.status(201).json({
    message: 'Product deleted successfully',
    products: allProducts
  });
});
