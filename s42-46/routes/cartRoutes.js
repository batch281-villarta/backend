const express = require('express');
const router = express.Router();

const cartController = require('../controllers/cartController');
const checkUser = require('../middlewares/checkUser');

router.use(checkUser);

router
  .route('/')
  .post(cartController.addToCart)
  .get(cartController.getCart)
  .delete(cartController.removeProducts)
  .patch(cartController.updateProducts);

module.exports = router;
