const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const verify = require('../middlewares/checkAdmin');
const checkUser = require('../middlewares/checkUser');

//User Routes
router.post('/register', userController.registerUser);
router.post('/login', userController.loginUser);

router.get('/:userId/userDetails', userController.getUserDetails);
router.get('/all', userController.getAllUserDetails);
router.get('/details', userController.getLoginUserDetails);
router.post('/checkout', checkUser, userController.checkout);
router.get('/orders', verify, userController.getOrders);
router.get('/myOrders', checkUser, userController.getMyOrder);
router.get('/:userId/setAsAdmin', verify, userController.setAdmin);
router.get('/:userId/removeAdmin', verify, userController.removeAdmin);

module.exports = router;
