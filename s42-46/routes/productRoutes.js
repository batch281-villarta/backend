const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');
const verify = require('../middlewares/checkAdmin');

// Verify all routes after this middleware
// router.use(verify);

router
  .route('/')
  .post(productController.addProduct)
  .get(productController.getAllActive);

router.get('/all', productController.getAllProducts);

router.get('/featured', productController.getFeaturedProducts);

router
  .route('/:productId')
  .get(productController.getProduct)
  .put(productController.updateProduct);

router.patch('/:productId/archive', productController.archiveProduct);
router.patch('/:productId/activate', productController.activateProduct);
router.patch('/:productId/feature', productController.featureProduct);
router.patch('/:productId/unfeature', productController.unfeatureProduct);
router.delete('/:productId/delete', productController.deleteProduct);

module.exports = router;
