const catchAsync = require('../utils/catchAsync');
const auth = require('./auth');
const AppError = require('../utils/appError');

module.exports = catchAsync(async (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData == null)
    return next(new AppError('Invalid authorization token'));

  if (!userData.isAdmin)
    return next(new AppError('You must be an administrator'));

  next();
});
