const mongoose = require('mongoose');

//middlewares
var validator = require('validator');

const userSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: [true, 'First Name is required'],
      trim: true
    },
    lastName: {
      type: String,
      required: [true, 'Last Name is required'],
      trim: true
    },
    email: {
      type: String,
      required: [true, 'Email is required'],
      validate: [validator.isEmail, '({VALUE}) is not a valid email']
    },
    mobileNumber: {
      type: Number,
      required: [true, 'Mobile Number is required'],
      trim: true
    },
    password: {
      type: String,
      required: [true, 'Password is required']
    },
    isAdmin: {
      type: Boolean,
      default: false
    },
    addedToCart: {
      products: [
        {
          productId: {
            type: mongoose.Schema.ObjectId
          },
          image: {
            type: String
          },
          productName: {
            type: String,
            required: [true, 'Product Name is required'],
            trim: true
          },
          description: {
            type: String,
            required: [true, 'Description is required'],
            trim: true
          },
          quantity: {
            type: Number,
            default: 0
          },
          price: {
            type: Number,
            default: 0
          },
          subTotal: {
            type: Number,
            default: 0
          }
        }
      ],
      totalAmount: {
        type: Number,
        default: 0,
        min: [0, 'Total should be a positive number']
      }
    },
    orderedProduct: [
      {
        products: [
          {
            productId: {
              type: mongoose.Schema.ObjectId
            },
            productName: {
              type: String,
              required: [true, 'Product Name is required'],
              trim: true
            },
            quantity: {
              type: Number,
              default: 0
            },
            image: {
              type: String
            },
            description: {
              type: String,
              trim: true
            },
            price: {
              type: Number,
              default: 0
            },
            subTotal: {
              type: Number,
              default: 0
            }
          }
        ],
        totalAmount: {
          type: Number,
          default: 0,
          min: [0, 'Total should be a positive number']
        },
        purchasedOn: {
          type: Date,
          default: new Date()
        }
      }
    ]
  },
  { id: false }
);

module.exports = mongoose.model('User', userSchema);
