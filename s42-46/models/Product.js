const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  image: {
    type: String,
    required: [true, 'Product image is required']
  },
  name: {
    type: String,
    required: [true, 'Product name is required'],
    trim: true
    // maxlength: [40, 'Product name is too long'],
    // minlength: [3, 'Product name is too short']
  },
  description: {
    type: String,
    required: [true, 'Description is required'],
    trim: true
  },
  price: {
    type: Number,
    default: 0,
    min: [1, 'Price should be a positive number']
  },
  isActive: {
    type: Boolean,
    default: true
  },
  isFeatured: {
    type: Boolean,
    default: false
  },
  createdOn: {
    type: Date,
    default: Date.now()
  },
  userOrders: [
    {
      userId: {
        type: mongoose.Schema.ObjectId
      },
      firstName: {
        type: String,
        trim: true
      },
      lastName: {
        type: String,
        trim: true
      },
      email: {
        type: String
      }
    }
  ]
});

module.exports = mongoose.model('Product', productSchema);
