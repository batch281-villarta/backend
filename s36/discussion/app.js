const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require('./routes/taskRoute');

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extented: true }));

const DB_STRING =
  'mongodb+srv://carolinovillartajr:lFQK1JPvlzP6KAsM@wdc028-course-booking.upk4zlw.mongodb.net/b281_to-do?retryWrites=true&w=majority';

mongoose
  .connect(DB_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('DB connection successfull'));

app.use('/tasks', taskRoute);

app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});
