// 2.

db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "fruitsOnSale" }]);

// 3.

db.fruits.aggregate([
  { $match: { stock: { $gte: 20 } } },
  { $count: "enoughStock" },
]);

// 4.

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      avg_price: { $avg: "$price" },
    },
  },
  { $sort: { _id: 1 } },
]);

// 5.

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      max_price: { $max: "$price" },
    },
  },
  { $sort: { _id: -1 } },
]);

// 6.

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      min_price: { $min: "$price" },
    },
  },
  { $sort: { _id: -1 } },
]);
