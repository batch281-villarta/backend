const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const DB_STRING =
  'mongodb+srv://carolinovillartajr:lFQK1JPvlzP6KAsM@wdc028-course-booking.upk4zlw.mongodb.net/b281_to-do?retryWrites=true&w=majority';

mongoose.connect(DB_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', console.error.bind());
db.once('open', () => console.log(`We're connected to the cloud database`));

const userSchema = new mongoose.Schema({
  username: String,
  password: String
});

const User = mongoose.model('User', userSchema);

app.post('/signup', (req, res) => {
  const { name, password } = req.body;

  User.findOne({ name: name }).then((result, err) => {
    let newUser = new User({
      name,
      password
    });

    newUser.save().then((saveUser, saveErr) => {
      if (saveErr) {
        return console.error(saveErr);
      } else {
        return res.status(201).send('New user registered');
      }
    });
  });
});

app.listen(port, () => console.log(`listening on port ${port}`));
