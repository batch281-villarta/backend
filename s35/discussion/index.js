const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const DB_STRING =
  'mongodb+srv://carolinovillartajr:lFQK1JPvlzP6KAsM@wdc028-course-booking.upk4zlw.mongodb.net/b281_to-do?retryWrites=true&w=majority';

mongoose.connect(DB_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', console.error.bind());
db.once('open', () => console.log(`We're connected to the cloud database`));

const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: 'pending'
  }
});

const Task = mongoose.model('Task', taskSchema);

app.get('/tasks', (req, res) => {
  Task.find({}).then((result, err) => {
    // If an error occured
    if (err) {
      // Will print any errors found in the console
      return console.log(err);
    }
    // If no errors are foud
    else {
      return res.status(200).json({
        data: result
      });
    }
  });
});

app.post('/tasks', (req, res) => {
  const { name } = req.body;

  Task.findOne({ name: name }).then((result, err) => {
    if (result !== null && result.name === name) {
      return res.send('Duplicate task found');
    } else {
      let newTask = new Task({
        name: name
      });

      newTask.save().then((saveTask, saveErr) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send('Task created successfully');
        }
      });
    }
  });
});

app.listen(port, () => console.log(`listening on port ${port}`));
