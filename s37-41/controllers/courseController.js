const auth = require('../auth');
const Course = require('../models/Course');

module.exports.addCourse = (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const { name, description, price } = req.body;

  console.log(isAdmin);

  if (!isAdmin) return res.send('You must be an administrator');

  let newCourse = new Course({
    name,
    description,
    price
  });

  newCourse.save();

  return res.send('Course added successfully');
};

module.exports.getAllCourses = async (req, res) => {
  return res.send(await Course.find({}));
};

module.exports.getAllActive = async (req, res) => {
  return res.send(await Course.find({ isActive: true }));
};

module.exports.getArchiveCourses = async (req, res) => {
  return res.send(await Course.find({ isActive: false }));
};

module.exports.getCourse = async (req, res) => {
  return res.send(await Course.findById(req.params.courseId));
};

module.exports.updateCourse = async (req, res) => {
  const { name, description, price } = req.body;

  let newCourse = {
    name,
    description,
    price
  };

  await Course.findByIdAndUpdate(req.params.courseId, newCourse);
  return res.send('Course updated successfully');
};

module.exports.archiveCourse = async (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;

  if (!isAdmin) return res.send('You must be an administrator');

  let archiveCourse = {
    isActive: false
  };
  await Course.findByIdAndUpdate(req.params.courseId, archiveCourse);
  return res.send('Course archived successfully');
};
