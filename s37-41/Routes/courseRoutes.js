const express = require('express');
const router = express.Router();

const courseController = require('../controllers/courseController');

router
  .route('/')
  .post(courseController.addCourse)
  .get(courseController.getAllActive);

router.route('/all').get(courseController.getAllCourses);
router.route('/archive').get(courseController.getArchiveCourses);

router
  .route('/:courseId')
  .get(courseController.getCourse)
  .put(courseController.updateCourse);

router.route('/:courseId/archive').patch(courseController.archiveCourse);

module.exports = router;
