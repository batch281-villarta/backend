// Using require directive to load http module
const http = require('http');

//Mock databse
const directory = [
  {
    name: 'Brandon',
    email: 'brandon@mail.com'
  },
  {
    name: 'Jobert',
    email: 'jobert@mail.com'
  }
];

const port = 4000;

const app = http.createServer((req, res) => {
  if (req.url === '/users' && req.method === 'GET') {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(directory));
    res.end();
  }

  if (req.url === '/users' && req.method === 'POST') {
    let requestBody = '';

    req.on('data', (data) => {
      requestBody += data;
    });

    req.on('end', () => {
      requestBody = JSON.parse(requestBody);

      const newUser = {
        name: requestBody.name,
        email: requestBody.email
      };

      directory.push(newUser);

      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.write(JSON.stringify(newUser));
      res.end();
    });
  }
});

app.listen(port, () => console.log(`Server is running at localhost:${port}`));
