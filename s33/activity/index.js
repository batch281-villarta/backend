//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

const TODO_URL = 'https://jsonplaceholder.typicode.com/todos';

async function fetchTodo(url = TODO_URL, callback) {
  const todo = await fetch(url, callback);
  const response = await todo.json();

  return response;
}

// Get Single To Do [Sample]
// async function getSingleToDo() {
//   return await fetch(TODO_URL)
//     .then((response) => response.json())
//     .then((json) => console.log(json));
// }

// Getting all to do list item
async function getAllToDo() {
  const result = await fetchTodo();
  const allTodo = result.map((todo) => todo.title);

  return allTodo;
}
getAllToDo().then((title) => console.log(title));

// [Section] Getting a specific to do list item
async function getSpecificToDo() {
  return await fetchTodo(`${TODO_URL}/1`);
}
getSpecificToDo().then((todo) => console.log(todo));

// [Section] Creating a to do list item using POST method
async function createToDo() {
  const postTodo = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      completed: false,
      title: 'Created Todo List Item',
      userID: 1
    })
  };

  return await fetchTodo(`${TODO_URL}`, postTodo);
}

createToDo().then((todo) => console.log(todo));

// [Section] Updating a to do list item using PUT method
async function updateToDo() {
  const updateTodo = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: 'Updated To Do List',
      description:
        'To update the my to do list with a different data structure',
      status: 'Pending',
      dateCompleted: 'Pending',
      userId: 1
    })
  };

  return await fetchTodo(`${TODO_URL}/1`, updateTodo);
}

updateToDo().then((todo) => console.log(todo));

// [Section] Deleting a to do list item
async function deleteToDo() {
  const deleteTodo = {
    method: 'DELETE'
  };
  return await fetchTodo(`${TODO_URL}/1`, deleteToDo);
}

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
    getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
    getSpecificToDo:
      typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
    createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
    updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
    deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null
  };
} catch (err) {}
