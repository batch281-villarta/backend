//console.log('Hello World!');

function addTwoNumbers(x, y) {
  console.log(`Displayed sum of ${x} and ${y}`);
  console.log(x + y);
}

addTwoNumbers(5, 15);

function subtractTwoNumbers(x, y) {
  console.log(`Displayed difference of ${x} and ${y}`);
  console.log(x - y);
}

subtractTwoNumbers(20, 5);

function multiplyTwoNumbers(x, y) {
  console.log(`The product of ${x} and ${y}`);
  return x * y;
}

const product = multiplyTwoNumbers(50, 10);
console.log(product);

function divideTwoNumbers(x, y) {
  console.log(`The quotient of ${x} and ${y}`);
  return x / y;
}

const quotient = divideTwoNumbers(50, 10);
console.log(quotient);

function areaOfCircle(radius) {
  const PI = Math.PI; //3.16

  console.log(`The result of getting the area of a circle with ${radius}:`);
  return PI * radius ** 2;
}

const circleArea = areaOfCircle(15);
console.log(circleArea);

function totalAverage(num1, num2, num3, num4) {
  console.log(`The average of ${num1}, ${num2}, ${num3} and ${num4}:`);
  return (num1 + num2 + num3 + num4) / 4;
}

const averageVar = totalAverage(20, 40, 60, 80);
console.log(averageVar);

function passingChecker(score, total) {
  const isPassed = score >= total * 0.75;

  console.log(`Is ${score}/${total} a passing score?`);
  return isPassed;
}

const isPassingScore = passingChecker(38, 50);
console.log(isPassingScore);
