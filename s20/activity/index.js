// console.log("Hello World");

//Objective 1
let string = "supercalifragilisticexpialidocious";
console.log(string);
let filteredString = "";

const vowels = ["a", "e", "i", "o", "u"];

for (let i = 0; i < string.length; i++) {
  if (!vowels.includes(string[i])) {
    filteredString += string[i];
  }
}

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    filteredString:
      typeof filteredString !== "undefined" ? filteredString : null,
  };
} catch (err) {}
